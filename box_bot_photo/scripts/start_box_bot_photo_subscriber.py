#! /usr/bin/env python3
import math
import os
import rclpy
from rclpy.node import Node
from rclpy.qos import ReliabilityPolicy, DurabilityPolicy, QoSProfile

from box_bot_photo.srv import TakePhoto
from rclpy.qos import QoSProfile
from rclpy.qos import QoSDurabilityPolicy
from rclpy.executors import MultiThreadedExecutor
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup
from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge  # Package to convert between ROS and OpenCV Images
import cv2  # OpenCV library


class Service(Node):

    def __init__(self, base_path="/home/user/ros2_ws/src/"):
        # Here we have the class constructor
        # call the class constructor to initialize the node as service_stop
        super().__init__('take_photo_node')

        self.photo_counter = 0
        self._base_path = base_path
        self.current_frame = None
        self.current_image_time = None
        self.declare_parameter('robot_name', "barista_GENERIC")
        self.getting_params()

        # Example : /box_bot_1/box_bot_1_camera/image_raw
        self.group1 = MutuallyExclusiveCallbackGroup()
        self.group2 = MutuallyExclusiveCallbackGroup()

        # Used to convert between ROS and OpenCV images
        self.br = CvBridge()

        image_topic_name = "/" + self.robot_name + "/" + \
            self.robot_name + "_camera"+"/image_raw"
        self.subscriber = self.create_subscription(
            Image,
            image_topic_name,
            self.image_callback,
            QoSProfile(depth=10, reliability=ReliabilityPolicy.RELIABLE,
                       durability=DurabilityPolicy.VOLATILE),
            callback_group=self.group1)

        # create the service server object
        take_photo_topic_name = self.robot_name + "/take_photo"
        self.subscriber = self.create_subscription(
            String,
            take_photo_topic_name,
            self.take_photo_callback,
            QoSProfile(depth=1),
            callback_group=self.group2)

    def getting_params(self):

        self.robot_name = self.get_parameter(
            'robot_name').get_parameter_value().string_value

        self.get_logger().info("## Take Photo robot_name ==="+str(self.robot_name))

    def take_photo_callback(self, msg):
        """
        Callback function.
        """
        if msg.data:
            path_of_photo = msg.data
            self.get_logger().info('>>>> Taking Photo for robot '+str(self.robot_name))
            self.save_latest_image(path_of_photo)
            self.get_logger().info('>>>> Taking Photo for robot '+str(self.robot_name)+" DONE")
        else:
            message = 'No path to save image given for '+str(self.robot_name)
            self.get_logger().error(message)

    def save_latest_image(self, save_path):
        """
        It saves the latest image of the box_bot_image topic into a jpeg
        """
        # Save your OpenCV2 image as a jpeg
        file_name = self.robot_name + "_" + \
            str(self.current_image_time) + ".jpeg"
        file_path = os.path.join(save_path, file_name)
        cv2.imwrite(file_path, self.current_frame)

        return True

    def image_callback(self, data, debug=False):
        """
        Callback function.
        """
        # Display the message on the console

        # Convert ROS Image message to OpenCV image
        self.current_frame = self.br.imgmsg_to_cv2(data, "bgr8")
        self.current_image_time = str(data.header.stamp.sec)
        # Display image
        if debug:
            self.get_logger().info('Receiving video frame')
            cv2.imshow(self.robot_name+"_camera", self.current_frame)
            cv2.waitKey(1)


def main(args=None):
    rclpy.init(args=args)
    try:
        battery_state_node = Service()

        num_threads = 2
        executor = MultiThreadedExecutor(num_threads=num_threads)
        executor.add_node(battery_state_node)

        try:
            executor.spin()
        finally:
            executor.shutdown()
            battery_state_node.destroy_node()

    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
