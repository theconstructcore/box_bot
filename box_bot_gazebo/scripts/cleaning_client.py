#! /usr/bin/env python3
import time
import rclpy
from rclpy.node import Node
from std_srvs.srv import SetBool


class Service(Node):

    def __init__(self):
        # Here we have the class constructor

        # call the class constructor to initialize the node as service_stop
        super().__init__('cleaning_client_node')

        self.declare_parameter('robot_name', "box_bot_GENERIC") 
        self.getting_params()     

        self.clean_server_name = "/"+self.robot_name + "/clean"

        self.get_logger().error("Starting Clientto clean server="+str( self.clean_server_name))

        self.client = self.create_client(
            SetBool, self.clean_server_name)
        while not self.client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info("service "+self.clean_server_name +
                                   "not available, waiting again...")

    def getting_params(self):

        self.robot_name = self.get_parameter(
            'robot_name').get_parameter_value().string_value

    def set_clean_state(self,  state):

        request_obj = SetBool.Request()
        request_obj.data = state
        self.future = self.client.call_async(request_obj)

        response_obj = SetBool.Response()

        if self.future.done():
            try:
                response_obj = self.future.result()
            except Exception as e:
                # Display the message on the console
                self.get_logger().error(
                    "Service "+self.set_entity_srv_name+"call failed ==>"+str(e))
            else:
                # Display the message on the console
                self.get_logger().info(
                    'Cleaning Status==>'+str(state))

        self.get_logger().error("Finished Clean Call...")

        return response_obj.success


def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    service = Service()
    service.set_clean_state(True)
    time.sleep(3)
    # service.set_clean_state(False)
    rclpy.shutdown()


if __name__ == '__main__':
    main()
