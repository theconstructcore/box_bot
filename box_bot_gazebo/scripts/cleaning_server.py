#! /usr/bin/env python3
import rclpy
from rclpy.node import Node

from std_msgs.msg import Float64MultiArray
from std_srvs.srv import SetBool



class Service(Node):

    def __init__(self):
        # Here we have the class constructor

        # call the class constructor to initialize the node as service_stop
        super().__init__('cleaning_server_node')

        self.declare_parameter('robot_name', "box_bot_GENERIC")
        self.getting_params()

        # create the service server object
        self.clean_server_name = self.robot_name + "/clean"
        self.srv = self.create_service(SetBool,
                                       self.clean_server_name,
                                       self.clean_clb)

        self.vel_cmd_topic_name = self.robot_name + \
            "/velocity_controller/commands"
        self.vel_cmd_publisher_ = self.create_publisher(
            Float64MultiArray, self.vel_cmd_topic_name, 1)
        
        self.get_logger().info("## Clean Server for robot===>"+str(self.robot_name)+"...READY")

    def getting_params(self):

        self.robot_name = self.get_parameter(
            'robot_name').get_parameter_value().string_value


    def clean_clb(self, request, response):

        if request.data:

            self.get_logger().info('Starting Cleaning')
            speed_msg = Float64MultiArray()
            speed_msg.data = [2.0]
            self.vel_cmd_publisher_.publish(speed_msg)
            response.success = True
            response.message = "Started Cleaning"
            

        else:
            self.get_logger().info('Stopped Cleaning')
            speed_msg = Float64MultiArray()
            speed_msg.data = [0.0]
            self.vel_cmd_publisher_.publish(speed_msg)
            response.success = True
            response.message = "Stopped Cleaning"

        # return the response parameter
        return response


def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    service = Service()
    # pause the program execution, waits for a request to kill the node (ctrl+c)
    rclpy.spin(service)
    # shutdown the ROS communication
    rclpy.shutdown()


if __name__ == '__main__':
    main()
